// inputmask
const form = document.querySelector('.form');
const telSelector = document.querySelector('input[type="tel"]');
const inputMask = new Inputmask('+7 (999) 999-99-99');
inputMask.mask(telSelector);

new window.JustValidate('.form', {
 rules:{  
    name:{
required: true,
minLength: 2,
maxLeght: 10
    },
tel:{
    required: true,
    function:() => {
        const phone = telSelector.inputMask.unputmask.unmaskedvalue();
        console.log(phone);
        return Number(phone) && phone.length === 10
        }
    },
 },
 colorWrong:'red',
 messages:{
    name:{
        required:'Введите имя',
        minLength:'Введите 3 и более символов',
        maxLeght:'Запрешено вводить более 15 символов'
    },
    email: {
        email: 'Введите коректный email',
        required: 'Введите email'
    },
    tel:{
        required:'Введите телефон',
        function:'Здесь должно быть 10 символов без +7'
    }
 },
submitHandler: function(trisForm){
}
})